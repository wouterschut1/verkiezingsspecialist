<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" href="assets/img/favicon.ico"/>
	<title>Verkiezingsspecialist</title>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' href='edd_templates/edd.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='assets/css/bootstrap.min.css' type='text/css' media='all'/>
		<!-- <link rel='stylesheet' href='assets/css/bootswatch.min.css' type='text/css' media='all'/> -->
	<link rel='stylesheet' href='assets/css/style.css' type='text/css' media='all'/>
	<link rel='stylesheet' href='assets/css/font-awesome.css' type='text/css' media='all'/>
	<!-- Ubuntu font  -->
	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

	<!-- UNCOMMENT THE FOLLOWING LINE TO ACTIVATE THE BOXED LAYOUT (see the documentation for more)-->

	<!--
	<link href="assets/css/boxed.css" rel="stylesheet">
	-->

	<!-- CHANGE THEME SKIN COLOR HERE (replace blue with any other color from assets/css/skins/ folder) -->
	<link href="assets/css/skins/red.css" rel="stylesheet">

</head>
<body>
	<div class="boxedlayout">

		<!-- HEADER
		================================================== -->
		<nav id="wow-menu" class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href='index.html'>
					<img alt="verkiezingsspecialist" id="logo-banner" src="assets/img/Logo_transparant_bg_tekst_2.png"/>
					</a>
				</div>
				<!-- Menu -->
				<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
					<ul id="menu-top" class="nav navbar-nav navbar-right">
						<li><a href="index.html">Home</a></li>
						<li><a href="over-ons.html">Over ons</a></li>
						<li><a href="lesmateriaal.html">Lesmateriaal</a></li>
					   <li class="dropdown"><a href="#">Verkiezingsmateriaal</a>
                            <ul class="dropdown-menu">
                                <li><a href="">Stemhokjes</a></li>
                                <li><a href="">Borden</a></li>
                                <li><a href="">Vlaggen</a></li>
                                <li><a href="">Potloden</a></li>
                            </ul>
                        </li>
                        <li class="active"><a href="contact.html">Contact</a>
					</ul>
				</div>
			</div>
			</div>
		</nav>

		<!-- PAGE CONTENT
		=================<div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
					<ul id="menu-top" class="nav navbar-nav navbar-right">
						<li><a href="index.html">Home</a></li>
						<li><a href="over-ons.html">Over ons</a></li>
						<li class="active"><a href="lesmateriaal.html">Lesmateriaal</a></li>
					   <li class="dropdown"><a href="#">Verkiezingsmateriaal</a>
                            <ul class="dropdown-menu">
                                <li><a href="">Stemhokjes</a></li>
                                <li><a href="">Borden</a></li>
                                <li><a href="">Vlaggen</a></li>
                                <li><a href="">Potloden</a></li>
                            </ul>
                        </li>
                        <li class="Lesmateriaal.html"><a href="contact.html">Contact</a>
					</ul>
				</div>
			</div>================================= -->

		<!-- TITLE BEGINS -->
		<div class="headerimage" style="background-image: url(assets/img/header.png);">
				<div class="headercontent">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="fleft">
									<h1>Cursus</h1>
								</div>
								<div class="fright breadc">
									<a href="index.html">Home</a> /
                                    <a href="Lesmateriaal.html">Lesmateriaal</a> /
                                    <a href="#">Cursus</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

<!-- <div class="headerimage homeimg text-center" style="background-image: url(assets/img/header-wide.jpg);">
			<div class="headercontent home">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h1 data-scrollreveal="enter top over 1.9s after 0.2s">Verkiezings specialist</h1>
							<p class="lead" data-scrollreveal="enter bottom over 1.9s after 0.4s">
								Dé specialist op het gebied van verkiezingen
							</p>
							<a target="_blank" data-scrollreveal="enter bottom over 1.0s after 1.3s" href="#startpositie" class="homeimgbtn">Over ons</a>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- TITLE ENDS -->

		<!-- MAIN CONTENT BEGINS -->

<div class="container padtop40 padbot50">
				<div class="row startpositie">
				<!-- MAIN CONTENT BEGINS -->
				<div class="col-md-12">
				<div class="row">
						 <div class="templatecontact">
           <!--  <iframe class="gmap" style="width:100%; height:480px; margin-top:70px; border:0px;" src="https://maps.google.com/maps?daddr=Hollywood,+Los+Angeles,+CA&#038;hl=en&#038;ll=34.138149,-118.353052&#038;spn=0.001912,0.002642&#038;geocode=FQk3CAIdq3Ly-Cm_eVIEB7_CgDHk-r2XZ5p69g&#038;t=h&#038;dirflg=r&#038;ttype=dep&#038;date=02%2F27%2F14&#038;time=6:27pm&#038;noexp=0&#038;noal=0&#038;sort=def&#038;mra=ls&#038;z=19&#038;iwloc=lyrftr:h,15266446442330102385,34.138236,-118.353481&#038;start=0&amp;output=embed">
            </iframe> -->
            <div class="container padtop20 padbot50">
                <div class="row">
                        <div class="col-md-7">
                            <div class="titleborder left">
                                <div>
                                    <h4 class="padbot10">Kom in contact</h4>
                                </div>
                            </div>

                                <div class="done">
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        Uw bericht is ontvangen, bedankt!
                                    </div>
                                </div>
                                <form method="post" action="contact.php" id="contactform">
                                    <div class="form">
                                        <input class="col-md-6" type="text" name="name" placeholder="Naam">
                                        <input class="col-md-6" type="text" name="email" placeholder="E-mail">
                                        <textarea class="col-md-12" name="comment" rows="7" placeholder="Bericht"></textarea>
                                        <input type="submit" id="submit" class="btn btn-primary" value="Verstuur">
                                    </div>
                                </form>
                        </div>
                        <div class="col-md-5">
                            <div class="titleborder left">
                                <div>
                                    <h4 class="padbot10">Onze gegevens</h4>
                                </div>
                            </div>
                            <!-- <i class="fa fa-map-marker"></i> <i>Location</i>: 2536 Zamora Road, Missisipi, 74C<br/><br/> -->
                            <!-- <i class="fa fa-clock-o"></i> <i>Hours</i>: Monday &#8211; Friday, 09:00 &#8211; 17:00<br/><br/> -->
                            <i class="fa fa-mobile-phone"></i> &nbsp;<i>Mobiel</i>: +31 612 345 6789<br/><br/>
                            <i class="fa fa-phone"></i> <i>Telefoon</i>: 1234 56 78 90<br/><br/>
                            <i class="fa fa-envelope"></i> <i>E-mail</i>: <a href="mailto:info@verkiezingsspecialist.nl">info@verkiezingsspecialist.nl</a>
                        </div>

                </div>
            </div>
        </div>
                        <!-- MAIN CONTENT ENDS -->
                        <!-- SIDEBAR BEGINS -->
                        <!-- <div class="col-md-3">
                            <div id="secondary" class="widget-area" role="complementary">
                            	<aside id="search-3" class="widget widget_search">
                            	<h1 class="widget-title section-title"><span>Search in Blog</span></h1>
                            	<form role="search" method="get" class="search-form" action="#">
                            		<input type="search" class="search-field" placeholder="Type and hit enter..." value="" name="s" title=""/>
                            		<input type="hidden" name="post_type" value="post"/>
                            	</form>
                            	</aside><aside id="text-2" class="widget widget_text">
                            	<h1 class="widget-title section-title"><span>About Kailo</span></h1>
                            	<div class="textwidget">
                            		<p>
                            			You are previewing "Kailo", a premium HTML bootstrap template that will help you design your ecommerce website.
                            		</p>
                            		<p>
                            			The Wordpress version provides a fully functional shop ready.
                            		</p>
                            	</div>
                            	</aside><aside id="recent-posts-3" class="widget widget_recent_entries">
                            	<h1 class="widget-title section-title"><span>Latest Posts</span></h1>
                            	<ul>
                            		<li>
                            		<a href="#">Embed Audio</a>
                            		</li>
                            		<li>
                            		<a href="#">Image Alignement</a>
                            		</li>
                            		<li>
                            		<a href="#">Embed Videos</a>
                            		</li>
                            		<li>
                            		<a href="#">Text Alignement</a>
                            		</li>
                            		<li>
                            		<a href="#">HTML Tags and Formatting</a>
                            		</li>
                            	</ul>
                            	</aside>
                            </div>
                            </div> -->
                        <!-- SIDEBAR ENDS -->
                    </div>
                </div>
            </div>
				<!-- MAIN CONTENT ENDS -->

				</div>
			</div>
		</div>

		<!-- MAIN CONTENT ENDS -->

		<!-- CALL TO ACTION
		================================================== -->
		<!-- <div class="actionbeforefooter text-center">
			<div class="container">
				<span><i class="fa fa-star fa-spin"></i> Psst! Like what you see? Enter code <b>WOWKAILO10</b> and get 10% discount for Kailo template. This week only! </span>
				<a target="_blank" class="actionbutton" href="#">Buy Now </a>
			</div>
		</div> -->

		<!-- FOOTER
		================================================== -->
		<footer class="themefooter section medium-padding bg-graphite">
		<div class="container">
			<div class="section-inner row">
				<div class="column column-1 col-md-4 rightbd">
					<div class="widgets">
						<div class="widget widget_text">
							<div class="widget-content">
								<h3 class="widget-title">Verkiezingsspecialist</h3>
								<div class="textwidget">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam cursus semper dolor vel tempus. Sed venenatis, magna dapibus laoreet convallis, sem sem fringilla metus, eget dictum sapien turpis eget justo. Cras eu dolor eu turpis eleifend rhoncus id sit amet tellus.
								</div>
							</div>
							<div class="clear">
							</div>
						</div>
					</div>
				</div>
				<!-- /footer-a -->
				<div class="column column-1 col-md-4 rightbd">
					<div class="widgets">
						<div class="widget widget_wysija">
							<div class="widget-content">
								<h3 class="widget-title">Meld je aan voor onze nieuwsbrief</h3>
								<div class="widget_wysija_cont">
									<form class="widget_wysija">
										 Wees als eerste op de hoogte met door te abonneren op onze nieuwsbrief.
										<p class="wysija-paragraph">
											<label>Email <span class="wysija-required">*</span></label>
											<input type="text" name="" class="wysija-input validate[required,custom[email]]" title="Email" value=""/>
										</p>
										<input class="wysija-submit wysija-submit-field" type="submit" value="Aanmelden"/>
									</form>
								</div>
							</div>
							<div class="clear">
							</div>
						</div>
					</div>
				</div>
				<!-- /footer-b -->
				<div class="column column-1 col-md-4">
					<div class="widgets">
						<div class="widget widget_edd_categories_tags_widget">
							<div class="widget-content">
								<h3 class="widget-title">Winkelcategorieën</h3>
								<ul class="edd-taxonomy-widget">
									<li><a href="#">Lesmateriaal</a></li>
									<li><a href="#">Verkiezingsmateriaal</a></li>
								</ul>
							</div>
							<div class="clear">
							</div>
						</div>
					</div>
				</div>
				<!-- /footer-c -->
				<div class="clear">
				</div>
			</div>
			<!-- /footer-inner -->
		</div>
		<!-- /grid -->
		<div class="sectioncredits">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<span class="credits-left fleft">
						2019 &copy; Alle rechten voorbehouden, verkiezingsspecialist </span>
						<ul class="footermenu fright">
							<p>Deze website is ontwikkeld door <a href="https://wsdev.nl" target="_blank">WSDEV</a></p>

						</ul>
					</div>
					<div class="clear">
					</div>
				</div>
			</div>
			<!-- /grid -->
		</div>
		<!-- /sectioncredits -->
		</footer>
		<!-- /footer -->
		<script type='text/javascript' src='assets/js/jquery.js'></script>
		<script type='text/javascript' src='assets/js/bootstrap.min.js'></script>
		<script type='text/javascript' src='assets/js/masonry.js'></script>
		<script type='text/javascript' src='assets/js/imagesloaded.js'></script>
		<script type='text/javascript' src='assets/js/SmoothScroll.js'></script>
		<script type='text/javascript' src='assets/js/init.js'></script>
		<script type='text/javascript' src='assets/js/anim.js'></script>
	</div>
</body>
</html>